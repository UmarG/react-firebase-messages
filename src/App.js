import React, {Component} from 'react';
import MessageList from './components/MessageList';
import Header from './components/Header';
import MessageBox from './components/MessageBox';
import {firebaseInit} from './components/Firebase';
import firebase from 'firebase';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    firebaseInit();
  }
  render() {
    return (
      <div className="container">
        <Header title="Simple Firebase App" />
        <div className="columns">
          <div className="column is-3" />
          <div className="column is-6">
            <MessageList db={firebase} />
          </div>
        </div>
        <div className="columns">
          <div className="column is-3" />
          <div className="column is-6">
            <MessageBox db={firebase} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
